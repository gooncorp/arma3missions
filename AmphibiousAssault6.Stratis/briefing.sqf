if (!isDedicated && (isNull player)) then
{
    waitUntil {sleep 0.1; !isNull player};
};

	
	
		player createDiaryRecord ["Diary",["Scripting Special Features","
		<br/>
		<br/>(Control + Shift + E) to use knife attack on enemy.
		<br/>
		<br/>Boat can be moved by driver if stuck on beach. Driver gets out and executes move boat forward or move boat backward scrollwheel actions.
		<br/>
		<br/>Control shift f1 open menu to see options.
		<br/>Some of these features will disabled for the primary server.



	"]];	
	
	player createDiaryRecord ["Diary",["MISSION NOTES","
	
		<br/>Victory is achieved by completing the mission as indicated.
		<br/>
		<br/>
		<br/> End Conditions (any one of these conditions will end the mission):
		<br/> - 100% of Enemy Force killed (Victory 1).
		<br/> - Intelligence recovered and extracted (Victory 2).
		<br/> - 100% of Friendly Forces killed (Failure).
		<br/>
		<br/> To obtain intelligence look for briefcase inside town of porto and search enemy bodies for ""retrieve intelligence"" action.
		<br/>
		<br/> Medical:
		<br/> - Cse medical system.
		<br/> - Everyone has basic medical equipment, medic has all medical equipment.
		<br/>
		<br/> Modules:
		<br/> - Cse BackBlast
		<br/> - Cse Medical
		<br/> - Cse WeaponRest
		<br/> - Cse Nightvision
		<br/>
		<br/> Intent: Recover intelligence leading to the location of a pow camp.
		<br/>
		<br/>
		<br/> Credits:
		<br/> - A mission by Gooncorp with assistance from Darksidesix.
		<br/> - Original framework.
		<br/> - Software assistance from Emerald.
		<br/> - Testers: Madplan, Broadkiller, Jimbo, Inigo_Montoya, Aistul, Darksidesix, Mixtrate, L3LTU3, War Unleashed, Briland
		<br/> - Modified spectator.sqf by TinfoilHate.
		<br/>
		<br/>


	"]];	
	

	player createDiaryRecord ["Diary",["APPENDIX A: Mission TO&E","

		<br/>A Company, 5th Special Forces Group, United States Army.
		<br/>	[
		<br/>	[-- 1 Platoon, 10 Personnel POW/RECON TASKING GROUP
		<br/>	[				2 Light Recon Teams 2 x 5 men
		<br/>	[				2 x CRRC Rafts	
			

	"]];

	player createDiaryRecord ["Diary",["COMMAND AND SIGNAL","

		<br/>1. Command 
		<br/>
		<br/>Bravo Company HQ located at US Airbase near the DMZ at firebase Barbara.
		<br/>
		<br/>2. Signals
		<br/>
		<br/>a. Radio Frequencies
		<br/>
		<br/>Short Range:
		<br/>
		<br/>Nil
		<br/>
		<br/>Long Range: (AN/PRC-148) 2 x 148 long range Radios for Team Leaders.
		<br/>
		<br/>  1 Platoon Net   - Channel 1, 31.775
		<br/>  Company Net     - Channel 5, 47.350
		<br/>  Battalion Fire Support Net - Channel 6, 51.850 (USS Jarett Warship  FFG30 Operating Outside DMZ)
		<br/>
		<br/>b. Codewords
		<br/>
		<br/>Nil
		<br/>
		<br/>c. Special Signals
		<br/>
		<br/>Nil
		<br/>
		<br/>d. Callsigns
		<br/>
		<br/>
		<br/>  Lurker 1 	- 1 Team
		<br/>  Lurker 2 	- 2 Team
		<br/>
		<br/>
		<br/>e. Passwords
		<br/>
		<br/>Nil
	"]];

			player createDiaryRecord ["Diary",["ATTACHMENTS/DETACHMENTS","

<br/>See Table of Organization and Equipment. (TOE)
		<br/>
		<br/>
		<br/>	
	"]];
			
	player createDiaryRecord ["Diary",["SERVICE SUPPORT","
	
		<br/>1. Support
		<br/>
		<br/>a. Fires
		<br/>Nil
		<br/>b. Ammo 
		<br/>Basic ammo inside CRRC craft.
		<br/>
		<br/>2. Service
		<br/>
		<br/>a. General
		<br/>
		<br/>Service and Support is not available because of your location - You will be on an island behind enemy lines.
		<br/>
		<br/>b. Handling of Prisoners
		<br/>
		<br/>If you have to take a prisoner restrain them, leave them in place and get out of there.
		<br/>
		<br/>Your job will not be to take prisoners on this mission. Use discretion.
		
	"]];

	player createDiaryRecord ["Diary",["EXECUTION","
		
		<br/>Intent Statement: It is the commander's intent that the mission be completed swiftly, with minimal friendly casualties - avoid contact with large enemy forces.  This is a volunteer mission and 10 individuals from one of our teams has signed up for the job. 
		<br/>
		<br/>1. Concept of the Operation
		<br/>
		<br/> Alpha platoon is to conduct a recon raid a small island occupied by enemy vietcong forces in an attempt to recover extremely valuable intelligence that may save the lives of several pows in the area.
		<br/>
		<br/>2. Maneuver
		<br/>
		<br/>Mission commander's discretion.
		<br/>
		<br/>3. Timing
		<br/>
		<br/>Mission commander's discretion.
		<br/>
		<br/>4. Tasks to Maneuver Units
		<br/>
		<br/>a. A Company:
		<br/>
		<br/>1 platoon shall conduct special operations against the town of Porto Banoi.
		<br/>
		<br/>Use standard recon manuevers.
		<br/>

	"]];
	
	
	player createDiaryRecord ["Diary",["MISSION","
		<br/>5TH SFG shall attempt to locate extremely valuable intelligence documents inside the city of Porto Bac.  There is a high ranking VC captain inside the town proper with a briefcase.  Search all the bodies for evidence - once located extract.
	"]];
	
	player createDiaryRecord ["Diary",["iii. Terrain and Weather","
		<br/>1. Terrain
		<br/>
		<br/>Terrain on the island is fairly sparse and open with small service roads and hamlets.  There are several masonry walls in the area that may serve as a decent cover.
		<br/>
		<br/>2. Obstacles
		<br/>
		<br/>There are some low lying shaparel and rocks as well as trees that may get in our way.  There is limited cover on this island since we are in northern part of Vietnam the jungle is not as thick.
		<br/>
		<br/>3. Key Terrain
		<br/>
		<br/>The dominant terrain features include hilltops that overlook built up areas, main supply routes, and large breaks in rocky areas.
		<br/>
		<br/>4. Weather
		<br/>
		<br/>Visibility is moderate due to the warm weather, winds are light, approximately 5-8km/h. Cloud cover is light. Temperature is approximately hot, 37 degrees celsius with moderate humidity.
	"]];
	player createDiaryRecord ["Diary",["ii. Enemy","
		<br/>1. Enemy Forces
		<br/>
		<br/>a. Disposition:
		<br/>
		<br/>The enemy disposition in this area is expected to be about a company size.  Vc guerrilas from several units are operating in this area as it is a clandestine staging area for attacks on Arvn forces.
		<br/>
		<br/>b. Composition:
		<br/>
		<br/>Enemy forces operating in our area of operations consist of hamlet guerrilas and a possible nva cadre unit.
		<br/>
		<br/>c. Strength:
		<br/>
		<br/>Enemy force strength is estimated to be approximately a reinforced company of viet-cong. The communists have been building up their forces on the island for the last 2 months.  They have decided to up their attacks and have taken several units in the area captive.  We believe they are sending these captured soldiers to prison camps in the nearby area for interrogation.
		<br/>
		<br/>d. Most Probable Course of Action
		<br/>
		<br/>Enemy forces trying to defend their island against our raid.
		<br/>
		<br/>e. Most Dangerous Course of Action
		<br/>
		<br/>Enemy forces fix and neutralise friendly force with concentrated return fire and aggressive maneuvering.
		<br/>
		<br/>CONG FORCES:
		<br/><img image='pics\picture2.jpg' width='512' height='512' />
		<br/> 

	"]];
	player createDiaryRecord ["Diary",["i. Friendly","
		<br/>2. Friendly Forces
		<br/>
		<br/>a. Disposition:
		<br/>
		<br/>Friendly forces consist of 2 special operations teams.  You are the only friendly forces in the area for 80km.  You are on your own.
		<br/>
		<br/>b. Higher Units Mission:
		<br/>
		<br/>nil
		<br/>
		<br/>c. Composition:
		<br/>
		<br/>1 Platoon minus from Alpha Company.
		<br/>
		<br/>See Appendix A for detailed Organization.
		<br/>
		<br/>d. Strength:
		<br/>
		<br/>1 Platoon forces are at full strength.
		<br/>
		<br/><img image='pics\picture5.jpg' width='512' height='512' /><br/>
	"]];
	
	player createDiaryRecord ["Diary",["SITUATION","

<br/><img image='pics\picture1.jpg' width='512' height='512' />
<br/>
<br/>
Our 2nd Lieutenant looks nervous.
<br/>
<br/>
We have found Charlie.  He is located on an island.  We have a mission to complete.
<br/>
<br/>
We are going to be in combat with a large enemy force come tomorrow.   We must infiltrate a large enemy camp in an attempt to gain intelligence.
<br/>
<br/>
This unit has been training together for close to 6 months now, we can do this.
<br/>
<br/>
Disco!
		<br/>
		<br/>
		<br/>	
	"]];



