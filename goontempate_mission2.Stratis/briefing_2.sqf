if (!isDedicated && (isNull player)) then
{
    waitUntil {sleep 0.1; !isNull player};
};

	
	
		player createDiaryRecord ["Diary",["Scripting Special Features","
		<br/>
		<br/>(Control + Shift + E) to use knife attack on enemy.
		<br/>
		<br/>Boat can be moved by driver if stuck on beach. Driver gets out and executes move boat forward or move boat backward scrollwheel actions.
		<br/>
		<br/>Control shift f1 open menu to see options.
		<br/>Some of these features will disabled for the primary server.



	"]];	
	
	player createDiaryRecord ["Diary",["MISSION NOTES","
	
		<br/>Victory is achieved by completing the mission as indicated.
		<br/>
		<br/>
		<br/> End Conditions (any one of these conditions will end the mission):
		<br/> - 100% of Enemy Force killed (Victory 1).
		<br/> - 100% of Friendly Forces killed (Failure).
		<br/>
		<br/>
		<br/> Medical:
		<br/> - Ace medical system.
		<br/> - Everyone has basic medical equipment, medic has all medical equipment.
		<br/>
		<br/>
		<br/> Intent: Destroy all enemies around coastal town of Ban Hoi.
		<br/>
		<br/>
		<br/> Credits:
		<br/> - A mission by Gooncorp.
		<br/> - Original framework.
		<br/> - Software assistance from Emerald.
		<br/> - Testers: Morluck, Sir Mills, HeroicLarvy, William, Darksidesix
		<br/> - Modified spectator.sqf by TinfoilHate.
		<br/>
		<br/>


	"]];	
	

	player createDiaryRecord ["Diary",["APPENDIX A: Mission TO&E","

		<br/>A Company, 5th Special Forces Group, United States Army.
		<br/>	[
		<br/>	[-- 1 Platoon, 12 Personnel POW/RECON TASKING GROUP
		<br/>	[				1 Light Recon Teams
			

	"]];

	player createDiaryRecord ["Diary",["COMMAND AND SIGNAL","

		<br/>1. Command 
		<br/>
		<br/>Bravo Company HQ located at US Airbase near the DMZ at firebase Barbara.
		<br/>
		<br/>2. Signals
		<br/>
		<br/>a. Radio Frequencies
		<br/>
		<br/>Short Range:
		<br/>
		<br/>Nil
		<br/>
		<br/>Long Range: (AN/PRC-148) 
		<br/>
		<br/>Nil
		<br/>
		<br/>b. Codewords
		<br/>
		<br/>Nil
		<br/>
		<br/>c. Special Signals
		<br/>
		<br/>Nil
		<br/>
		<br/>d. Callsigns
		<br/>
		<br/>
		<br/>  Lurker 1 	- 1 Team
		<br/>
		<br/>
		<br/>e. Passwords
		<br/>
		<br/>Nil
	"]];

			player createDiaryRecord ["Diary",["ATTACHMENTS/DETACHMENTS","

<br/>See Table of Organization and Equipment. (TOE)
		<br/>
		<br/>
		<br/>	
	"]];
			
	player createDiaryRecord ["Diary",["SERVICE SUPPORT","
	
		<br/>1. Support
		<br/>
		<br/>a. Fires
		<br/>Nil
		<br/>b. Ammo 
		<br/>Nil
		<br/>
		<br/>2. Service
		<br/>
		<br/>a. General
		<br/>
		<br/>Service and Support is not available because of your location - You will be on an island behind enemy lines.
		<br/>
		<br/>b. Handling of Prisoners
		<br/>
		<br/>If you have to take a prisoner restrain them, leave them in place and get out of there.
		<br/>
		<br/>Your job will not be to take prisoners on this mission. Use discretion.
		
	"]];

	player createDiaryRecord ["Diary",["EXECUTION","
		
		<br/>Intent Statement: It is the commander's intent that the mission be completed swiftly, with minimal friendly casualties - avoid contact with large enemy forces.  This is a volunteer mission and 12 individuals from one of our units has signed up for the job. 
		<br/>
		<br/>1. Concept of the Operation
		<br/>
		<br/> Alpha platoon is to conduct a recon raid a small town occupied by Vietcong forces.
		<br/>
		<br/>2. Maneuver
		<br/>
		<br/>Mission commander's discretion.
		<br/>
		<br/>3. Timing
		<br/>
		<br/>Mission commander's discretion.
		<br/>
		<br/>4. Tasks to Maneuver Units
		<br/>
		<br/>a. A Company:
		<br/>
		<br/>1 platoon shall conduct special operations against the town of Ban Hoi.
		<br/>
		<br/>Use standard recon manuevers.
		<br/>

	"]];
	
	
	player createDiaryRecord ["Diary",["MISSION","
		<br/>5TH SFG shall attempt to sweep and clear the port town of Ban Hoi.
	"]];
	
	player createDiaryRecord ["Diary",["iii. Terrain and Weather","
		<br/>1. Terrain
		<br/>
		<br/>Terrain on the island is fairly sparse and open with small service roads and hamlets.  There are several masonry walls in the area that may serve as a decent cover.
		<br/>
		<br/>2. Obstacles
		<br/>
		<br/>There are some low lying shaparel and rocks as well as trees that may get in our way.  There is limited cover on this island since we are in northern part of Vietnam the jungle is not as thick.
		<br/>
		<br/>3. Key Terrain
		<br/>
		<br/>The dominant terrain features include hilltops that overlook built up areas, main supply routes, and large breaks in rocky areas.
		<br/>
		<br/>4. Weather
		<br/>
		<br/>Visibility is moderate due to the warm weather, winds are light, approximately 2-4km/h. Cloud cover is light. Temperature is approximately hot, 37 degrees celsius with moderate humidity.
	"]];
	player createDiaryRecord ["Diary",["ii. Enemy","
		<br/>1. Enemy Forces
		<br/>
		<br/>a. Disposition:
		<br/>
		<br/>The enemy disposition in this area is expected to be about a platoon size.  Vc guerrilas from several units are operating in this area as it is a clandestine staging area for attacks on Arvn forces.
		<br/>
		<br/>b. Composition:
		<br/>
		<br/>Enemy forces operating in our area of operations consist of hamlet guerrilas and a possible nva cadre unit.
		<br/>
		<br/>c. Strength:
		<br/>
		<br/>Enemy force strength is estimated to be approximately a reinforced platoon of Viet-Cong.
		<br/>
		<br/>d. Most Probable Course of Action
		<br/>
		<br/>Enemy forces trying to defend their island against our raid.
		<br/>
		<br/>e. Most Dangerous Course of Action
		<br/>
		<br/>Enemy forces fix and neutralise friendly force with concentrated return fire and aggressive maneuvering.
		<br/>
		<br/>CONG FORCES:
		<br/><img image='pics\picture2.jpg' width='512' height='512' />
		<br/> 

	"]];
	player createDiaryRecord ["Diary",["i. Friendly","
		<br/>2. Friendly Forces
		<br/>
		<br/>a. Disposition:
		<br/>
		<br/>Friendly forces consist of 1 12 man special operations team.  You are the only friendly forces in the area for 80km.  You are on your own.
		<br/>
		<br/>b. Higher Units Mission:
		<br/>
		<br/>nil
		<br/>
		<br/>c. Composition:
		<br/>
		<br/>1 Platoon minus from Alpha Company.
		<br/>
		<br/>See Appendix A for detailed Organization.
		<br/>
		<br/>d. Strength:
		<br/>
		<br/>1 Platoon forces are at full strength.
		<br/>
		<br/><img image='pics\picture5.jpg' width='512' height='512' /><br/>
	"]];
	
	player createDiaryRecord ["Diary",["SITUATION","

<br/><img image='pics\picture1.jpg' width='512' height='512' />
<br/>
<br/>
We have found the location of many hidden Vietcong camps after raiding Porto.
<br/>
<br/>
We know what Charly is doing, after hitting this coastal camp we can attempt to locate the pow camp in the area.
<br/>
<br/>
We are going to be in combat with the enemy tomorrow.
<br/>
<br/>
This unit has been training together for close to 6 months now, we can do this.
<br/>
<br/>
Disco!
		<br/>
		<br/>
		<br/>	
	"]];



