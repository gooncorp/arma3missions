if (!isserver) exitwith {};
_p = 0;
{
   if ((side _x) == East && alive _x) then {
    _p = _p + 1;
   };
} forEach allUnits;
if (_p > 120) exitwith {};
_pos = position (_this select 0);
_area = position (_this select 1);


_grp=creategroup east;
"JO_Insurgent_R2" CreateUnit [ _pos ,_grp,"szldier1 = this",.4,"sergeant"];
sleep .04;
_randomizer = (ceil random 12);
for "x" from 0 to (2 + _randomizer) do {
"JO_Insurgent_R2" CreateUnit [_pos,_grp,"szldier_gis = this",.4,"private"];
sleep .04;
};

{
_x addEventHandler ["killed", {_this spawn gooncorp_deletebody}];
if (random 1 < .3) then {
[_x,"insurgent5a"] call gooncorp_fnc_globalgear;} else {
[_x,"insurgent6a"] call gooncorp_fnc_globalgear;
};
} foreach units _grp;
sleep 4;

[{
[  "textures\vietnam_vc_goon.paa", "textures\nva.jpg"] call gooncorp_aitextures_mod1;

},"BIS_fnc_spawn",true,true] call BIS_fnc_MP;


(leader _grp) move position (_this select 1);

sleep 60;

_fairway = _grp addWaypoint [position (_this select 1), 0];
_fairway setWaypointType "move";
_fairway setWaypointBehaviour "combat";
if (random 1 < .5) then {
_fairway setWaypointFormation "LINE";} else {
_fairway setWaypointFormation "WEDGE";};










