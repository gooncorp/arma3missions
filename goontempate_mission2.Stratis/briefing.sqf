
if (isDedicated) exitWith { };

	waitUntil {!(isNull player)};
	waitUntil {player==player};

	player createDiaryRecord ["Diary",["MISSION NOTES","
	
		<br/>Victory is achieved by completing the mission as indicated.
		<br/>
		<br/>This mission is scalable meaning that enemies are procedurally spawned based on the amount of players connected. This mission can be played with 10 players all the way up to 50 players. 
		<br/>
		<br/> End Conditions:
		<br/> - 80% of Enemy Force killed.
		<br/> - 60% of Friendly Forces killed.
		<br/>
		<br/> Medical:
		<br/> - Only Medics can use Epi and Morphine.
		<br/> - Anyone can use Bandages.
		<br/> - Must use field hospital for full heal.
		<br/> - LRRP Patrolman are qualified CLS.
		<br/>
		<br/> Credits:
		<br/> - A Vietnam mission created and scripted by Goon/Gooncorp.
		<br/>
		<br/> - Help from DarkSideSix.  
		<br/>
		<br/> - Spectator script by HeinBloed.  
		<br/>
		<br/> - Beta-testing help from DarkSideSix, War Unleashed, BerkTheTurk, Two Face, Besiden, Best2nd, Saikai, and Enex.
		<br/>
		<br/> - Approved by E Kin.
		<br/>
		<br/><img image='pics\vietnam.jpg' width='512' height='512' /><br/>
	"]];	

	player createDiaryRecord ["Diary",["APPENDIX B: F4 Phantom Notes","

		<br/>F4B Phantom II CAS is located at the US Airbase on the Runway.
		<br/>
		<br/>Select FAB 250 Munitions on the aircraft for Napalm Bomb.
		<br/>
		<br/>Blast radius for napalm is between 60 and 120 meter safe distance.
		<br/>
		<br/>Resupply from Container on Runway.
	"]];	

	player createDiaryRecord ["Diary",["APPENDIX A: Mission TO&E","

		<br/>B Company, 1st Battalion, 5th Cavalry Regiment.
		<br/>	[
		<br/>	[-- 2 Platoon, 37 Personnel
		<br/>	[				X3 M923A2, 2 Light Infantry Squads, 1 ARVN Infantry Squad (Attached), 1 HQ Squad
		<br/>	[
		<br/>	[-- 4 Platoon, 6 Personnel
		<br/>	[				X1 Landrover, 1 LRRP Squad
		<br/>	[				X3 M1030
		<br/>	[				X1 CRRC Raft
		<br/>	[
		<br/>C Company, 227th Assault Helicopter Battalion (Light UH-1)	
		<br/>	[
		<br/>	[-- 1st Lift Helicopter Platoon, 6 Personnel
		<br/>	[				X2 UH-1 Iroquois, 2 Aircrew Teams
		<br/>	[				X3 M923A2 Rearm, Repair, Refuel Trucks
		<br/>		
		<br/>	[-- ---------, 1 Personnel
		<br/>	[				X1 F4B Phantom II
	"]];

	player createDiaryRecord ["Diary",["COMMAND AND SIGNAL","

		<br/>1. Command 
		<br/>
		<br/>Bravo Company HQ located at US Airbase.
		<br/>
		<br/>2. Signals
		<br/>
		<br/>a. Radio Frequencies
		<br/>
		<br/>Short Range:
		<br/>
		<br/>Nil
		<br/>
		<br/>Long Range: (AN/PRC-77)
		<br/>
		<br/>  2 Platoon Net   - Channel 2, 31.775
		<br/>  Company Net     - Channel 5, 47.350
		<br/>  Battalion Fire Support Net - Channel 6, 51.850
		<br/>
		<br/>b. Codewords
		<br/>
		<br/>Nil
		<br/>
		<br/>c. Special Signals
		<br/>
		<br/>Nil
		<br/>
		<br/>d. Callsigns
		<br/>
		<br/>  Blackjack 2'0 	- 2 Platoon HQ
		<br/>  Blackjack 2'1 	- A Squad
		<br/>  Blackjack 2'2 	- B Squad
		<br/>  Blackjack 2'3 	- C Squad ARVN
		<br/>  Blackjack 2'6 	- 2 Platoon Leader
		<br/>  Blackjack 2'7 	- 2 Platoon Sergeant
		<br/>
		<br/>  Blackjack 4'1 	- LRRP Squad
		<br/>  
		<br/>  Raider 1'1 	    - 1 UH1 
		<br/>  Raider 1'2 	    - 2 UH1
		<br/>  Phoenix 1'1 	  - F4 Phantom II CAS
		<br/>
		<br/>e. Passwords
		<br/>
		<br/>Nil
	"]];
			
	player createDiaryRecord ["Diary",["SERVICE SUPPORT","
	
		<br/>1. Support
		<br/>
		<br/>a. Fires
		<br/>
		<br/>Napalm Bombs provided by the F4B Phantom II Fighter Bomber.
		<br/>
		<br/>2. Service
		<br/>
		<br/>a. General
		<br/>
		<br/>Service and Support is available at the US Airbase. Each vehicle and helicopter has extra medical supplies and small arms ammunition. The crate at the US Airbase contains ammunition and weapons along with medical supplies.
		<br/>
		<br/>b. Handling of Prisoners
		<br/>
		<br/>Restrain and transport to GRID 064 070 and turn over to 1st Battlion MP Detachment to hold for processing.
		
	"]];

	player createDiaryRecord ["Diary",["EXECUTION","
		
		<br/>Intent Statement: It is the commander's intent that the mission be completed swiftly, with minimal friendly casualties and a high enemy body count.
		<br/>
		<br/>1. Concept of the Operation
		<br/>
		<br/> Bravo Company is to Clear and Hold the region of Quang Tri Province and destroy any enemy presence to allow safe construction of a base in the highlands.
		<br/>
		<br/>2. Maneuver
		<br/>
		<br/>Mission commander's discretion.
		<br/>
		<br/>3. Timing
		<br/>
		<br/>Mission commander's discretion.
		<br/>
		<br/>4. Tasks to Maneuver Units
		<br/>
		<br/>a. B Company:
		<br/>
		<br/>2 Platoon shall Clear and Hold the region of Quang Tri Province and eliminate any enemy presence.
		<br/>
		<br/>4 Platoon LRRP shall conduct reconnaissance in the region of Quang Tri Province to provide bravo company timely intelligence for thier operation as well as engage the enemy with CAS assets and ambush techniques.
		<br/>
		<br/>b. C company 227th:
		<br/>
		<br/>1st Lift Helicopter Platoon will support bravo companys mission by providing lift transport and MEDEVAC as needed.
		<br/>
		<br/>c. F4B Phantom II:
		<br/>
		<br/>Phantom 1'1 to provide Close Air Support to bravo company as needed.
	"]];
	
	
	player createDiaryRecord ["Diary",["MISSION","
		<br/>2 Platoon shall Clear and Hold the region of Quang Tri Province by conducting fighting patrols in order to locate, disrupt and render the enemy unable to launch coordinated attacks on friendly forces moving into the area.
	"]];
	
	player createDiaryRecord ["Diary",["iii. Terrain and Weather","
		<br/>1. Terrain
		<br/>
		<br/>Terrain in the highlands is extremely restrictive heavy jungle, providing a high level of concealment for light infantry but lack of mobility for vehicles. Terrain in the general area is largely moderate jungle canopy, consisting of gently sloping farmland and ricepattys interspersed with light formations of straw hamlets and dirt service roads.
		<br/>
		<br/>2. Obstacles
		<br/>
		<br/>The enemy has been known to employ mines and improvised explosives as well as booby-traps leading up to built up areas. It is unknown if the enemy has positioned any obstacles in the interior of the jungle.
		<br/>
		<br/>3. Key Terrain
		<br/>
		<br/>The dominant terrain features include hilltops that overlook built up areas, main supply routes, and large breaks in forested areas.
		<br/>
		<br/>4. Weather
		<br/>
		<br/>Visibility is poor due to fog and rain, Winds are medium, approximately 15-20km/h. Cloud cover is moderate. Temperature is approximately 35 degrees celsius with moderate humidity.
	"]];
	player createDiaryRecord ["Diary",["ii. Enemy","
		<br/>1. Enemy Forces
		<br/>
		<br/>a. Disposition:
		<br/>
		<br/>The enemy disposition in this area is unknown but expected to be lightly scattered throughout.
		<br/>
		<br/>b. Composition:
		<br/>
		<br/>Enemy forces operating in our area of operations consist of NVA regulars along with Vietcong belonging to an unknown unit. The enemy force is moderately well trained and highly motivated, but poorly equipped. Enemy force is equipped with Soviet-pattern small arms as well as captured friendly weapons. With the exception of a possible 12.7mm weapon in the area, enemy force likely has little to no static anti-tank or anti-air assets, though have been known to engage helicopters with small arms and MMG. Enemy artillery or mortar fire is highly unlikely.
		<br/>
		<br/>c. Strength:
		<br/>
		<br/>Enemy force strength is estimated to be approximately a reinforced infantry platoon of NVA with potential Vietcong squads operating in the outlying areas.
		<br/>
		<br/>d. Most Probable Course of Action
		<br/>
		<br/>Enemy forces defending their positions with hasty defenses and employing hit and run guerilla tactics against friendly forces.
		<br/>
		<br/>e. Most Dangerous Course of Action
		<br/>
		<br/>Enemy forces fix and neutralise friendly force with concentrated ambushes and coordinated deliberate attacks.
		<br/>
		<br/>NVA FORCES:
		<br/><img image='pics\nva.jpg' width='256' height='256' />
		<br/> 
		<br/>VIETCONG FORCES:
		<br/><img image='pics\vietcong.jpg' width='256' height='256' />
	"]];
	player createDiaryRecord ["Diary",["i. Friendly","
		<br/>2. Friendly Forces
		<br/>
		<br/>a. Disposition:
		<br/>
		<br/>Bravo Company HQ is located at US Airbase at GRID 064 070, 1 and 2 Platoon are co-located and are preparing to conduct operations, 3 Platoon is currently combat ineffective following a prior engagement and are located at China Beach recuperating and conducting R and R.
		<br/>
		<br/>b. Higher Units Mission:
		<br/>
		<br/>MACV is planning to build a base in the highlands west of the DMZ in and around GRID 038,068. Bravo Company has been tasked with clearing the surrounding countryside before Engineers can start the construction project.
		<br/>
		<br/>c. Composition:
		<br/>
		<br/>2 Platoon Minus from Bravo Company, with attached ARVN Squad and LRRP Team as well as support aviation.
		<br/>
		<br/>See Appendix A for detailed Organization.
		<br/>
		<br/>d. Strength:
		<br/>
		<br/>2 Platoon forces are at full strength complemented by ARVN Forces.
		<br/>
		<br/><img image='pics\arvntroops.jpg' width='256' height='256' /><br/>
	"]];
	
	player createDiaryRecord ["Diary",["SITUATION","
<br/>
<br/>
The new ARVN were ready before anyone else, looks like they have something to prove.  LRRP have been in and out of S2 briefs for the last 2 days or so.  All the platoon commander has been talking about between brass meetings is kill count.
<br/>
<br/>
Charlie is on the move on the mountain. Civilians are reportedly fleeing the area as fast as they can, which means that something is about to happen.  
<br/>
<br/>
Our platoon is ready for operations - our vehicles are fueled up and ammo has been handed out.  
<br/>
<br/>
Our goal is to clear the AO of all hostile forces.
<br/>
<br/>
We gonna come on in for the big win.
		<br/>
		<br/>
		<br/>	
	"]];