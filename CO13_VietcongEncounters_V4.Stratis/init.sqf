#include "framework\gooncorp_functions.sqf";
if (isMultiplayer) then {
titlecut ["Please wait for the mission to initialize...", "black faded", 25];};



// gooncorps arma 3 framework
// all written by gooncorp
// you can use this code with permission

uo_a3_nameHUD_active = true;
movingalready = false;
uo_a3_nameHUD_enabled = true;
debug = false;
playinganimation = false;


//call gooncorp_vki;



gooncorp_coop_scoresimple_local =
	{
	if (!isserver) exitwith {};
	ended = false;
		while {true } do {
			enemyCount = 0;
			friendlyCount = 0;
				{
				if (side _x == east && alive _x && _x distance area1 < 250) then {
					enemyCount = enemyCount + 1;
				};
				if (side _x == west && alive _x && _x distance specpen > 200 && isPlayer _x) then {
					friendlyCount = friendlyCount + 1;
				};
			} foreach allunits;
		if (friendlyCount == 0 && !ended) then {
		ended = true;
		[{_handle = [] spawn gooncorp_warriorending_lose;},"BIS_fnc_spawn",true,true] call BIS_fnc_MP;
		};
		if (enemyCount < (_this select 0) && !ended && time > 1200) then {
		ended = true;
		[{_handle = ["end2"] spawn gooncorp_warriorending_win;},"BIS_fnc_spawn",true,true] call BIS_fnc_MP;	
		};
	sleep 10;
	if (random 1 < .2) then {
	[{call gooncorp_jipcatchup;},"BIS_fnc_spawn",true,true] call BIS_fnc_MP;
	};
	};
};


gooncorp_preinit = {





[proclusionarea2] call gooncorp_light;

if (initialized) exitwith {hint "already ran init!";};
initialized = true;
_handle = [] spawn gooncorp_jip;
sleep 11;
//call gooncorp_assignboatpositions;
//f (isMultiplayer) then {
	//if ((paramsarray select 0) == 0) then {
	//["africa"] call gooncorp_setaiidentity;// call with africa or vietnam so far
	//};
	//if ((paramsarray select 1) == 0) then {
	//
	//};
	//};
	
sleep 4;
call gooncorp_flashlights;
[pole1] call gooncorp_light;	
["vietnam"] call gooncorp_setaiidentity;// call with africa or vietnam so far
call gooncorp_addreducearmor;
//call gooncorp_boataction;
[area1, 1000, .3, 58] call gooncorp_destroybuildingsinradius_vietnamhuts;
call gooncorp_removeallclothing;
//call gooncorp_playergearscript;

call gooncorp_spreadenemies;
sleep 1;
call gooncorp_playertextures;
sleep 2;
call gooncorp_blackin;
[pilotstart] call gooncorp_jipaircraft;
//_handle = [] spawn gooncorp_enemyweapons;
if (isMultiplayer) then {
_handle = [4] spawn gooncorp_coop_scoresimple_local;};
_handle = [] spawn gooncorp_debug;
_handle = [] spawn gooncorp_keypress;
sleep 6;
["textures\vietnam_vc_goon.paa"] call gooncorp_aitextures;


disableUserInput false;



};


























////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// actual init starts here
call gooncorp_fog;
initialized = false;
//[spawnarea, 655] call gooncorp_destroybuildingsinradius ;
//_handle = [] spawn gooncorp_briefingcustominsertion;// emergency custom insertion removed for this version
_handle = [] spawn gooncorp_preinit;
//call briefing;
execVM "briefing.sqf";

//player addEventHandler ["killed", {_this spawn gooncorp_respawnevent}];

 