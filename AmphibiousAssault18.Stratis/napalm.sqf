//gooncorp
//2013

debug = false;

plane = (_this select 0);
// 190 138 256 // 130 198 245
ved = velocity plane;
_tracker = true; 
_bullet = _this select 6; 
_dir=getdir _bullet; 
_planedir = getdir plane;
_oldpos = []; 

//////
bombtype = "bomb_04_f";// change this for different bomb
//////
if ((_this select 4) != bombtype) exitwith {};//emergency change this back
//hint format ["is %1", _this select 4];












private ["_oldpos"];

while {not (isnull _bullet)} do { _oldpos = getposASL _bullet; sleep .01;};

				_slight6 = "#lightpoint" createVehicleLocal [ _oldpos select 0, _oldpos select 1, 10];
				_slight6 setlightBrightness 5.4;
				_slight6 setlightAmbient[.3, .1, 0];
				_slight6 setlightColor[.3, .1, 0];

			   _color = [1, 1, 1];




			   _obj = (vehicle player);
			   _pos = [_oldpos select 0, _oldpos select 1, 5];
 
               //--- Dust
                       setwind [0.401112*2,0.204166*2,false];
               _velocity = wind;
               _color = [.13, .2, .4];
               _alpha = 0.05 + random 0.12;
               _ps2 = "#particlesource" createVehicleLocal _pos;  
               _ps2 setParticleParams [["a3\data_f\ParticleEffects\Universal\Universal.p3d", 16, 12, 8, 0], "", "Billboard", 1, 74, [0, 0, 10], _velocity, 1, 1.175, 1, 0, [45 + (random 111)], [_color + [0], _color + [_alpha], _color + [0]], [1000], 1, 0, "", "", 1];
               _ps2 setParticleRandom [3, [4 + (random 10), 4 + (random 10), 45 + random 35], [random 2, random 2, 0], 1, 0, [0, 0, 0, 0.01], 0, 0];
               _ps2 setParticleCircle [0.1, [0, 0, 0]];
               _ps2 setDropInterval 0.2;

               _ps9 = "#particlesource" createVehicleLocal _pos;  
               _ps9 setParticleParams [["a3\data_f\ParticleEffects\Universal\Universal.p3d", 16, 12, 5, 0], "", "Billboard", 1, 15, [5, 5, 2], _velocity, 1, 1.8, 0, 0, [.01 + (random .01)], [_color + [0], _color + [_alpha], _color + [0]], [1000], 1, 1, "", "", 1];
               _ps9 setParticleRandom [0, [1 + (random 1), 4 + (random 1), 5 + random 5], [25, 25, 53], 14, 3, [0, 0, 0, 22], 1, 0];
               _ps9 setParticleCircle [0.1, [5, 5, 5]];
               _ps9 setDropInterval 0.001;

               _ps3 = "#particlesource" createVehicleLocal _pos;  
               _ps3 setParticleParams [["a3\data_f\ParticleEffects\Universal\Universal.p3d", 16, 4, 8, 0], "", "Billboard", 1, 74, [0, 0, 12], _velocity, 1, 1.175, 1, 0, [25 + (random 111)], [_color + [0], _color + [_alpha], _color + [0]], [1000], 1, 0, "", "", 1];
               _ps3 setParticleRandom [3, [1 + (random 1), 4 + (random 1), 45 + random 35], [random 2, random 2, 0], 1, 0, [0, 0, 0, 4], 0, 0];
               _ps3 setParticleCircle [0.1, [0, 0, 0]];
               _ps3 setDropInterval 0.4;
_color = [1, 1, 1];
_alpha = 0.35 ;
               _ps4 = "#particlesource" createVehicleLocal _pos;  
               _ps4 setParticleParams [["a3\data_f\ParticleEffects\Universal\Universal.p3d", 16, 1, 12, 0], "", "Billboard", 1, 2 + random 3, [0, 0, 5], _velocity, 1, 1, 1, 0, [.3 + (random 1)], [_color + [0], _color + [_alpha], _color + [0]], [1000], 1, 0, "", "", 1];
               _ps4 setParticleRandom [3, [1 + (random 1), 4 + (random 1), 75 + random 125], [random 4, random 4, 2], 14, 3, [0, 0, 0, .1], 1, 0];
               _ps4 setParticleCircle [0.1, [0, 0, 0]];
               _ps4 setDropInterval 0.02;
 

               _ps7 = "#particlesource" createVehicleLocal _pos;  
               _ps7 setParticleParams [["a3\data_f\ParticleEffects\Universal\Universal.p3d", 16, 3, 12, 0], "", "Billboard", 1, 1 + random 1, [5, 5, 4], _velocity, 1, 1, 1, 0, [.5 + (random .5)], [_color + [0], _color + [_alpha], _color + [0]], [1000], 1, 0, "", "", 1];
               _ps7 setParticleRandom [3, [1 + (random 1), 4 + (random 1), 75 + random 125], [random 5, random 2, 1], 14, 3, [0, 0, 0, .1], 1, 0];
               _ps7 setParticleCircle [0.1, [0, 0, 0]];
               _ps7 setDropInterval 0.002;
               sleep 3 + (random 2);

_alpha = 0.15 ;

               _ps8 = "#particlesource" createVehicleLocal _pos;  
               _ps8 setParticleParams [["a3\data_f\ParticleEffects\Universal\Universal.p3d", 16, 12, 3, 0], "", "Billboard", 1, 2 + random 4, [5, 5, 10], _velocity, 1, 1, 1, 1, [.01 + (random .01)], [_color + [0], _color + [_alpha], _color + [0]], [1000], 1, 1, "", "", 1];
               _ps8 setParticleRandom [0, [15 + (random 15), 15 + (random 15), 15 + random 15], [25, 25, 53], 14, 3, [0, 0, 0, 12], 1, 0];
               _ps8 setParticleCircle [0.1, [5, 5, 5]];
               _ps8 setDropInterval 0.2;

_alpha = 0.08 ;
               _ps10 = "#particlesource" createVehicleLocal _pos;  
               _ps10 setParticleParams [["\A3\data_f\ParticleEffects\Universal\Refract",1,0,1], "", "Billboard", 1, 3, [1, 1, 1], _velocity, 1, 1, 1, 1, [.1 + (random .1)], [_color + [0], _color + [_alpha], _color + [0]], [1000], 1, 1, "", "", 1];
               _ps10 setParticleRandom [0, [5, 5, 5], [25, 25, 53], 14, 3, [0, 0, 0, 0], 1, 0];
               _ps10 setParticleCircle [0.1, [5, 5, 5]];
               _ps10 setDropInterval .1;


_color = [1, 1, 0];
_velocity = [10, 10, 35];

sleep .05;

deletevehicle _ps9;
sleep 1 + (random 2);
deletevehicle _ps8;
_units = nearestobjects [_pos, ["All", "", "House", "Wall"], 100];
{
_ps7 setpos getpos _x;
_ps4 setpos getpos _x;
_x setdammage 2;
sleep .5;
} foreach _units;
_ps7 setpos _pos;
_ps4 setpos _pos;
_delay = 16 + random 16;
sleep _delay;
{
_x setdammage 2;
sleep .5;
} foreach _units;
deletevehicle _ps10;
deletevehicle _ps7;
deletevehicle _ps8;
_delay = 16 + random 16;
sleep _delay;
deletevehicle _ps2;
{
_x setdammage 2;
hideobject _x;

sleep .5;
} foreach _units;
deletevehicle _ps3;
deletevehicle _ps4;
sleep 15;
deletevehicle _slight6;


