if (!isDedicated && (isNull player)) then
{
    waitUntil {sleep 0.1; !isNull player};
};

	
	
		player createDiaryRecord ["Diary",["Scripting Special Features","
		<br/>
		<br/>(Control + Shift + E) to use knife attack on enemy.
		<br/>
		<br/>
		<br/>
		<br/>Control shift f1 open menu to see options.
		<br/>Some of these features will disabled for the primary server.



	"]];	
	
	player createDiaryRecord ["Diary",["MISSION NOTES","
	
		<br/>Victory is achieved by completing the mission as indicated.
		<br/>
		<br/>
		<br/> End Conditions (any one of these conditions will end the mission):
		<br/> - 70% of Enemy Force killed (Victory 1).
		<br/> - 100% of Friendly Forces killed (Failure).
		<br/>
		<br/>
		<br/> Medical:
		<br/> - Ace medical system.
		<br/> - Medic has all medical equipment.
		<br/>
		<br/>
		<br/> Intent: Attrit enemy positions at center of Island, locate hidden NVA company.
		<br/>
		<br/>
		<br/> Credits:
		<br/> - A mission by Gooncorp.
		<br/> - Original framework.
		<br/> - Software assistance from Emerald.
		<br/> - Testers: FlyingSpaghettiMonster, U83r, Jimbo, Miles_Teg
		<br/> - Modified spectator.sqf by TinfoilHate.
		<br/>
		<br/>


	"]];	
	

	player createDiaryRecord ["Diary",["APPENDIX A: Mission TO&E","

		<br/>A Company, 27th MEU
		<br/>	[
		<br/>	[-- 1 Platoon, 52 Personnel Amphibious Assault Group
		<br/>	[            1 x Hq Team 
		<br/>	[            3 x Assault Team
		<br/>	[            1 x Lrrp Team
		<br/>	[            3 x Flight Crew
	        <br/>	[            6 x M113 M2
	        <br/>	[            1 x Huey
		<br/>	[            2 x Chinook
			

	"]];

	player createDiaryRecord ["Diary",["COMMAND AND SIGNAL","

		<br/>1. Command 
		<br/>
		<br/>Bravo Company HQ located at USS WASP (LHD).
		<br/>
		<br/>2. Signals
		<br/>
		<br/>a. Radio Frequencies
		<br/>
		<br/>Short Range:
		<br/>
		<br/>Nil
		<br/>
		<br/>Long Range: (AN/PRC-148) 
		<br/>
		<br/>Nil
		<br/>
		<br/>b. Codewords
		<br/>
		<br/>Nil
		<br/>
		<br/>c. Special Signals
		<br/>
		<br/>Nil
		<br/>
		<br/>d. Callsigns
		<br/>
		<br/>
		<br/>Alligator 1-1 1st Marine Strike Team
		<br/>Alligator 1-2 2nd Marine Strike Team
                <br/>Alligator 1-3 3rd Marine Strike Team
		<br/>Stalker   1-1 LRRP Team
		<br/>Panther   1-1 F4 Phantom
		<br/>Bird 1        Pilot UH1 #1
		<br/>Bird 2        Pilot Chinook #1	
		<br/>Bird 2        Pilot Chinook #2	
		<br/>Hotel 1-6     Hq Team		
		<br/>
		<br/>
		<br/>
		<br/>e. Passwords
		<br/>
		<br/>Nil
	"]];

			player createDiaryRecord ["Diary",["ATTACHMENTS/DETACHMENTS","

<br/>See Table of Organization and Equipment. (TOE)
		<br/>
		<br/>
		<br/>	
	"]];
			
	player createDiaryRecord ["Diary",["SERVICE SUPPORT","
	
		<br/>1. Support
		<br/>
		<br/>a. Fires
		<br/>Nil
		<br/>b. Ammo 
		<br/>There is ammo on the ship as well as loaded into the vehicles.
		<br/>
		<br/>2. Service
		<br/>
		<br/>a. General
		<br/>
		<br/>Service and Support will be on the USS Wasp.  Ammo, repair, and refuel facilites available for flight crews on the starboard stern of ship.
		<br/>
		<br/>b. Handling of Prisoners
		<br/>
		<br/>Take no prisoners.
		<br/>
		<br/>Your job will not be to take prisoners on this mission. Use discretion.
		
	"]];

	player createDiaryRecord ["Diary",["EXECUTION","
		
		<br/>Intent Statement: It is the commander's intent that the mission be completed swiftly, with minimal friendly casualties.  This is a sweep and clear mission intended to disrupt large scale enemy operations in the area.
		<br/>
		<br/>1. Concept of the Operation
		<br/>
		<br/> Alpha platoon is to conduct reconnaisance on the Island of Ban Boc.  Locate the enemy forces on the center of Island, close with them, and destroy them.
		<br/>
		<br/>2. Maneuver
		<br/>
		<br/>Mission commander's discretion.
		<br/>
		<br/>3. Timing
		<br/>
		<br/>Mission commander's discretion.
		<br/>
		<br/>4. Tasks to Maneuver Units
		<br/>
		<br/>a. A Company:
		<br/>
		<br/>Alpha Company shall conduct sweep and clear operations on the island of Ban Boc.
		<br/>
		<br/>
		<br/>

	"]];
	
	
	player createDiaryRecord ["Diary",["MISSION","
		<br/>Marines Expeditionary Force conduct all out assault on island of Ban Boc.
	"]];
	
	player createDiaryRecord ["Diary",["iii. Terrain and Weather","
		<br/>1. Terrain
		<br/>
		<br/>Terrain on the island is fairly sparse and open with small service roads and hamlets.  There are several masonry walls in the area that may serve as a decent cover.
		<br/>
		<br/>2. Obstacles
		<br/>
		<br/>There are some low lying shaparel and rocks as well as trees that may get in our way.  There is limited cover on this island since we are in northern part of Vietnam the jungle is not as thick.
		<br/>
		<br/>3. Key Terrain
		<br/>
		<br/>The dominant terrain features include hilltops that overlook built up areas, main supply routes, and large breaks in rocky areas.
		<br/>
		<br/>4. Weather
		<br/>
		<br/>Visibility is poor due to the warm weather, winds are light, approximately 2-4km/h. There is thick fog.. Temperature is approximately hot, 37 degrees celsius with moderate humidity.
	"]];
	player createDiaryRecord ["Diary",["ii. Enemy","
		<br/>1. Enemy Forces
		<br/>
		<br/>a. Disposition:
		<br/>
		<br/>The enemy disposition in this area is expected to be about a Company size.  Vc guerrilas from several units are operating in this area as well as several large NVA squads.
		<br/>
		<br/>b. Composition:
		<br/>
		<br/>Enemy forces operating in our area of operations consist of hamlet guerrilas and NVA regulars.
		<br/>
		<br/>c. Strength:
		<br/>
		<br/>Enemy force strength is estimated to be approximately a reinforced company of Communist Fighters.
		<br/>
		<br/>d. Most Probable Course of Action
		<br/>
		<br/>Enemy forces trying to defend their island against our raid, enemy forces launch qrf counter-attack.
		<br/>
		<br/>e. Most Dangerous Course of Action
		<br/>
		<br/>Enemy forces fix and neutralise friendly force with concentrated return fire and aggressive maneuvering.
		<br/>
		<br/>CONG FORCES:
		<br/><img image='pics\picture2.jpg' width='512' height='512' />
		<br/> 

	"]];
	player createDiaryRecord ["Diary",["i. Friendly","
		<br/>2. Friendly Forces
		<br/>
		<br/>a. Disposition:
		<br/>
		<br/>Friendly forces consist of a 52 man Marine Corps and Navy Assault Group.  You will be launching from the USS Wasp (Carrier).  Lrrp team will start on the Island at 051, 047.
		<br/>
		<br/>b. Higher Units Mission:
		<br/>
		<br/>nil
		<br/>
		<br/>c. Composition:
		<br/>
		<br/>1 Company from Alpha Company.
		<br/>
		<br/>See Appendix A for detailed Organization.
		<br/>
		<br/>d. Strength:
		<br/>
		<br/>1 Company forces are at full strength.
		<br/>
		<br/><img image='pics\picture5.jpg' width='512' height='512' /><br/>
	"]];
	player createDiaryRecord ["Diary",["F4 Phantom / Napalm","

<br/><img image='pics\napalm.jpg' width='512' height='512' />
<br/> 4 x BLU 118 Fuel Bombs (Napalm)
<br/>
The napalm-b has a lethal range of 200 meters and it will burn for several minutes.  The white-phosphorous mixture helps penetration into the enemy musculature causing massive casualties.  Stay clear of this ordinance well after it goes off.
<br/>
<br/>
Disco!
		<br/>
		<br/>
		<br/>	
	"]];
	player createDiaryRecord ["Diary",["SITUATION","

<br/><img image='pics\picture1.jpg' width='512' height='512' />
<br/>
<br/>
After we destroy the enemy company located on this island we can attempt our first rescue of American pows which are known to be in the region.
<br/>
<br/>
We are going to be in combat with the enemy tomorrow.
<br/>
<br/>
This unit has been training together for close to 6 months now, we can do this.
<br/>
<br/>
Disco!
		<br/>
		<br/>
		<br/>	
	"]];


	player createDiaryRecord ["Diary",["Historical Overview","
	 

The Vietcong
<br/>
<br/>
The Vietnamese Communists, or Vietcong, were the military branch of the National Liberation Front (NLF), and were commanded by the Central Office for South Vietnam, which was located near the Cambodian border. For arms, ammunition and special equipment, the Vietcong depended on the Ho Chi Minh trail. Other needs were met inside South Vietnam. 
 

	
		
 vietcong Main force Vietcong units were uniformed, full-time soldiers, and were used to launch large scale offensives over a wide area. Regional forces were also full-time, but operated only within their own districts. When necessary, small regional units would unite for large scale attacks. If enemy pressure became too great, they would break down into smaller units and scatter.
Unlike the main troops, who saw themselves as professional soldiers, local Vietcong groups tended to be far less confident. For the most part, recruits were young teenagers, and while many were motivated by idealism, others had been pressured or shamed into joining. They also harbored real doubts about their ability to fight heavily armed and well-trained American soldiers.

Initially, local guerrillas were given only a basic minimum of infantry training, but if they were recruited to a main force unit, they could receive up to a month of advanced instruction. Additionally, there were dozens of hidden centers all over South Vietnam for squad and platoon leader, weapons and radio training. To ensure that the guerrillas understood why they were fighting, all training courses included political instruction.

By the mid-1960s, most main force Vietcong troops were armed with Chinese versions of the Russian AK-47 submachine gun. They also used a range of effective Soviet and Chinese light and medium machine guns, and infrequently, heavy machine guns. In particular, heavy machine guns were valued for defense against American helicopters.

For destroying armored vehicles or bunkers, the Vietcong had highly effective rocket propelled grenades and recoilless rifles. Mortars were also available in large numbers and had the advantage of being very easy to transport.

Many weapons, including booby traps and mines, were homemade in villages. The materials ranged from scavenged tin can to discarded wire, but the most important ingredients were provided by the enemy. In a year, dud American bombs could leave more than 20,000 tons of explosives scattered around the Vietnamese countryside. After air-raids, volunteers retrieved the duds and the dangerous business of creating new weapons began.

Local forces also designed primitive weapons, some designed to frighten intruders, but others were extremely dangerous. 'Punji traps' -- sharp spikes hidden in pits -- could easily disable an enemy soldier. Punjis were often deliberately contaminated to increase the risk of infection. 
 

 
The Vietcong were masters at moving through and blending into the local terrain
<br/>
<br/>
Guerrilla Tactics
tunnel In December 1965, Ho Chi Minh and the North Vietnamese leadership ordered a change in a way the war in the South was to be fought. From now on, the Vietcong would avoid pitched battles with the Americans unless the odds were clearly in their favor. There would be more hit and run attacks and ambushes. To counter the American build-up, Vietcong recruitment would be stepped up and more North Vietnamese Army troops would be infiltrated into South Vietnam.
The Vietcong, following the example of Chinese guerillas before them, had always given the highest priority to creating safe base areas. They were training grounds, logistics centers and headquarters. They also offered secure sanctuaries for times when the war might go badly.

Hiding the base areas had always been a high priority for the Vietcong. Now, with American spotter planes everywhere, it was more vital than ever to protect them. In remote swamps or forests, there were few problems, but nearer the capital, it was much more difficult. The answer was to build enormous systems of underground tunnels.

The orders coming from NLF headquarters were absolutely clear. Tunnels were not to be treated as mere shelters. They were fighting bases capable of providing continuous support for troops. Even if a village was in enemy hands, the NLF beneath were still able to conduct offensive operations.

There were complexes big and small scattered across the country. Each villager in a NLF area had to dig three feet of tunnel a day. There was even a standard handbook specifying how tunnels were to be built. The biggest tunnel systems were in the Iron Triangle and the Cu Chi District, only 20 miles from Saigon. 
 

 
An American soldier carefully examines a Vietcong tunnel--they were often boobytrapped if abandoned
Close-up: Cu Chi
chuchi The base area at Cu Chi was a vast network, with nearly 200 miles of tunnels. Any facility used by the guerillas -- a conference room or training area -- had almost immediate underground access. Hidden trapdoors led below, past guarded chambers, to long passages. At regular intervals, branches led back to the surface and other secret entrances. Some openings were even concealed beneath the waters of streams or canals.
At the deeper levels, there were chambers carved out for arms factories and a well for the base's water supply. There were store rooms for weapons anad rice, and there was sometimes a hospital or forward aid station. Long communication tunnels connected the base with other distant complexes.

Base kitchens were always near the surface, with long, carved-out chimneys designed to diffuse cooking smoke and release it some distance away. Near the kitchens were the guerilla's sleeping chambers, where they could survive for weeks at a time if need be. Everywhere on the top level, there were tunnels leading upwards to hundreds of hidden firing posts for defense of the base. 

<br/>
<br/>

<br/>

		<br/>
		<br/>
		<br/>	
	"]];
