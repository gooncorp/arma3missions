if (!isDedicated && (isNull player)) then
{
    waitUntil {sleep 0.1; !isNull player};
};

	
	
		player createDiaryRecord ["Diary",["Scripting Special Features","
		<br/>
		<br/>Control Shift - e to use knife attack on enemy.
		<br/>
		<br/>Control shift f1 open menu to see options.
		<br/>Some of these features will disabled for the primary server.
	"]];	
	
player createDiaryRecord ["Diary",["COMMAND AND SIGNAL","
 
<br/>1. Command...
<br/>
<br/>a. Higher Unit's Command Post:
<br/>
<br/>nil
<br/>
<br/>b. Key Personnel and CP during movement, at ORP, on OBJ:
<br/>
<br/>Mission Commander's discretion.
<br/>
<br/>c. Succession of Command:
<br/>
<br/>Mission Commander's discretion.
<br/>
<br/>2. Signal
<br/>
<br/>a. Frequencies
<br/>
<br/>Short Range:
<br/> nil
<br/>
<br/>Long Range:
<br/> Channel 1, 36.625 - 1 Platoon Net
<br/>
<br/>b. Callsigns
<br/>
<br/> Reckoner 11
<br/> Reckoner 12
<br/>
<br/>c. Pyrotechnics and Signals
<br/> Mission commander's discretion.
<br/>
<br/>d. Codewords
<br/> Mission commander's discretion.
<br/>
<br/>e. Challenge and Password
<br/> Challenge: nil
<br/> Password: nil
<br/>
<br/>f. Number Combination
<br/> Sum: 13
<br/>
<br/>g. Running Password
<br/> Running Password: nil
<br/>
<br/>h. Recognition Signals
<br/> Mission commander's discretion.
<br/>
<br/>i. Special Instructions for RTO
<br/> Mission commander's discretion.
 
"]];
 
player createDiaryRecord ["Diary",["SERVICE SUPPORT","
 
<br/>1. General
<br/>
<br/>a. Resupply Points:
<br/>
<br/>nil
<br/>
<br/>b. Casualty and Damaged Equipment:
<br/>
<br/> Mission commander's discretion.
<br/>
<br/>2. Material and Services
<br/>
<br/>a. Subsistence:
<br/>
<br/>nil
<br/>
<br/>b. Medical Evacuation:
<br/>
<br/> Mission commander's discretion.
<br/>
<br/>3. Personnel
<br/>
<br/>nil
 
"]];
 
player createDiaryRecord ["Diary",["EXECUTION","
 
<br/>Intent Statement: Close with and engage Moroccan rebel forces inside the town of Habel.
<br/>
<br/>1. Concept of the Operation
<br/>
<br/>Use offensive tactics to engage rebels located inside the town of Habel.  This is a standard MOUT operation.
<br/>
<br/>a. Maneuver:
<br/>
<br/> Mission commander's discretion.
<br/>
<br/>b. Tasks to Maneuver Units
<br/>
<br/> Mission commander's discretion.
<br/>
<br/>c. Coordinating Instructions:
<br/>
<br/> Mission commander's discretion.
<br/>
<br/>d. MOPP Level:
<br/>
<br/>MOPP Level is 0.
"]];
 
player createDiaryRecord ["Diary",["MISSION","
 
<br/>A large rebel force has been located inside the town of Habel with satellite imagry.  This force has been linked to attacks on American interests in the area.  We must eliminate this force.
 
"]];
 
player createDiaryRecord ["Diary",["SITUATION","
 
<br/>1. Enemy Forces
<br/>
<br/>a. Composition:
<br/>
<br/>Expected to be about a platoon of light infantry.  These units have been spotted with ak47 and rpk weapons.
<br/>
<br/>b. Disposition:
<br/>
<br/>Enemy rebel forces expected to be maneuvering through the town.
<br/>
<br/>c. Strength:
<br/>
<br/>Enemy forces are at full strength with medical attachments.
<br/>
<br/>d. Most Probable Course of Action
<br/>
<br/>Enemies use hit and run tactics to try and engage our forces from inside buildings and along alleyways.
<br/>
<br/>e. Most Dangerous Course of Action
<br/>
<br/>Enemies use ambush tactics to route our forces or render us combat ineffective.
<br/>
<br/>2. Friendly Forces
<br/>
<br/>a. Higher Units Mission:
<br/>
<br/>Battalion level command is operating 20 clicks south of us at US AIRBASE BARBARA.
<br/>
<br/>b. Composition:
<br/>
<br/>A platoon of light infantry. 2 Squads.
<br/>
<br/>c. Supporting Fires:
<br/>
<br/>nil
<br/>
<br/>3. Attachments and Detachments
<br/>
<br/>1 medic per squad.
"]];

